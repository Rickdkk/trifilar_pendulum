import glob
import os

import matplotlib.pyplot as plt
from scipy import signal
import worklab as wl
import numpy as np


def trifilar_calc(weight=0.8, radius=0.295, length=0.675, period=1):
    return (weight*9.81*radius**2*period**2)/(4*np.pi**2*length)


if __name__ == "__main__":
    # Settings
    m_wheel = 6.0  # mass of Optipush system
    r_wheel = 0.315  # radius of wheel
    l_rope = 0.675  # length of ropes
    data_location = ".\\example_data"

    os.chdir(data_location)
    files = glob.glob("inertia*")

    periods, inertias = [], []
    for file in files:
        data = wl.io.load(file)
        norm_data = data[:, 1, :] - data[:, 0, :]  # normalize to central marker
        time = np.linspace(0, data.shape[2]*0.01, data.shape[2])

        f, Pxx_den = signal.periodogram(norm_data[0, :], 100)
        periods.append(1/f[np.argmax(Pxx_den)])  # find frequency with max power, T=1/f

        (fig, [ax1, ax2]) = plt.subplots(2, 1)  # visual confirmation
        ax1.plot(time, norm_data[0, :], 'r', label="x")
        ax1.plot(time, norm_data[1, :], 'g', label="y")
        ax1.plot(time, norm_data[2, :], 'b', label="z")
        ax1.set_xlabel("time [s]")
        ax1.set_ylabel("position [mm]")
        ax1.legend(loc=1)
        ax2.semilogy(f[1:], Pxx_den[1:])
        ax2.set_xlabel("frequency [Hz]")
        ax2.set_ylabel("power [V**2/Hz]")

        plt.show()

    [inertias.append(trifilar_calc(weight=m_wheel, radius=r_wheel, period=pd, length=l_rope)) for pd in periods]
    [print(f"Moment of inertia = {J:.3f} kgm^2 for period of {T:.2f}s") for J, T in zip(inertias, periods)]
    print(f"Mean moment of inertia = {np.mean(inertias):.4f} kgm^2")
