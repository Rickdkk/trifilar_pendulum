# Trifilar pendulum example

The trifilar pendulum can be used to determine the moment of inertia of an object around an axis.

### Methodology

A simple trifilar pendular was constructed and suspended on top of a treadmill. 
The pendulum was leveled using a Mitutoyo angle sensor. 
The period of the pendulum oscillation is needed for the calculation and we used a marker system to track this oscillation.
The period is determined from the frequency components of the position signal.

### Reporting errors

If you find an error or mistake, please contact me or submit an issue through this page.

### Authors

* **Rick de Klerk** - *Initial work* - [gitlab](https://gitlab.com/rickdkk) - [UMCG](https://www.rug.nl/staff/r.de.klerk/)

### License

This project is licensed under the GNU GPLv3 - see the [LICENSE](LICENSE) file for details